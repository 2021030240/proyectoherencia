/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Herencia;

/**
 *
 * @author Administrator
 */
public class EmpleadoE extends Empleado{
    private float pagoH;
    private float horasT;
    
    public EmpleadoE(){
        this.pagoH=0.0f;
        this.horasT=0.0f;
    }

    public EmpleadoE(float pagoH, float horasT) {
        this.pagoH = pagoH;
        this.horasT = horasT;
    }

    public EmpleadoE(float pagoH, float horasT, int numEmpleado, String nombre, String puesto, String depto) {
        super(numEmpleado, nombre, puesto, depto);
        this.pagoH = pagoH;
        this.horasT = horasT;
    }
    

    public float getPagoH() {
        return pagoH;
    }

    public void setPagoH(float pagoH) {
        this.pagoH = pagoH;
    }

    public float getHorasT() {
        return horasT;
    }

    public void setHorasT(float horasT) {
        this.horasT = horasT;
    }
    
    
    
    @Override
    public float calcularPago(){
        return this.horasT * this.pagoH;
    }
}
