/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Herencia;

/**
 *
 * @author Administrator
 */
public class EmpleadoB extends Empleado implements Impuesto {
    
    protected float pagoD;
    protected float diasT;
    
    
    public EmpleadoB(){
        this.pagoD=0.0f;
        this.diasT=0.0f;
    }

    public EmpleadoB(float pagoD, float diasT, int numEmpleado, String nombre, String puesto, String depto) {
        super(numEmpleado, nombre, puesto, depto);
        this.pagoD = pagoD;
        this.diasT = diasT;
    }

    public float getPagoD() {
        return pagoD;
    }

    public void setPagoD(float pagoD) {
        this.pagoD = pagoD;
    }

    public float getDiasT() {
        return diasT;
    }

    public void setDiasT(float diasT) {
        this.diasT = diasT;
    }
    
    
    @Override
    public float calcularPago() {
       return this.diasT * this.pagoD;
    }
    
    @Override
    public float calcularImpuesto(){
        float impuestos=0;
        if(this.calcularPago()>5000) impuestos = this.calcularPago()*.16f;
        return impuestos;
    }
    
}
